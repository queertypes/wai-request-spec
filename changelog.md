# 0.10.2.0 (Sep. 10, 2015)

* stack support
* GHC 7.10

# 0.10.0.1 (May 29. 2015)

* handle decoding form parameter fields correctly
  * failed when content-type header contained char-encoding data

# 0.10.0.0 (May 29. 2015)

* toEnv now parses a request w/o form data
* toEnvRaw parses a request + bytestring body for form data
* toEnvRaw parses a request + [(Text, Text)] for form data

# 0.9.1.0 (May 28, 2015)

* Export Env type

# 0.9.0.1 (May 28, 2015)

* Remember to update changelog

# 0.9.0.0 (May 28, 2015)

## Features

* Add form parameter parsing

* If the content-type is application/x-www-form-urlencoded, then
  parameters are automatically parsed from the request body

## Interface Changes

* all query param functions xP are renamed to xQ
* new form param functions are named xF
* toEnv now takes ByteString (requestBody) parameter


# 0.8.5.2 (Apr. 29, 2015)

* Fix description in cabal file

# 0.8.5.1 (Apr. 29, 2015)

* Fix description in cabal file

# 0.8.5.0 (Apr. 29, 2015)

* Initial Hackage release
* Improved Error Show instance and new helpers:
  * toList to convert Error to [String]
  * toTextList to convert Error to [Text]
* Inline annotations for Parser internals
* Basic documentation

# 0.8.2.0

* Correct option combinators
  * `Maybe (P a)` to `P (Maybe a)`
  * Former construction made them impossible to use in P context

# 0.8.1.1

* Actually export new combinators

# 0.8.1.0

* Add parser combinators for optional inputs
  * int[PH]M, bool[PH]M, float[PH]M, text[PH]M, bytes[PH]M

# 0.8.0.0

* Reorganized internals/exports
  * Added RequestSpec.Combinators
  * Added RequestSpec.Parser
* Added derived combinators for handling data from headers/params
  * int[PH], bool[PH], float[PH], text[PH], bytes[PH]

# 0.7.0.1

* Remove RequestSpec.Internal as everything stable is re-exported in RequestSpec

# 0.7.0.0

* Moved away from using native HTTP rep towards Text based
  * HeaderName -> CI Text
  * Param      -> ParamValue (Text)
* Assumes utf8 encodings

# 0.6.1.0

* Changed representation of query params
  * Maybe ByteString -> ParamValue (ByteString)

# 0.6.0.0

* Brand new parser
  * Accumulating errors
  * Continuation passing style
  * Primitive and derived combinators
  * Functor, Applicative, Monad, Monad+, Alternative, Monoid instances
* fromParsed -> fromEnv
* Separate Spec step removed
  * The spec is now the sequence of combinators given in fromEnv

# 0.5.0.2

* This changelog came to be!
* Initially available on Gitlab
* Core functions in place:
  * Required and Optional, Headers and Params
  * Parsing thereof
* Lots of examples
  * Bare WAI/Warp
  * Scotty
  * Spock
