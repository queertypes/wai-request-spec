{-|
Module      : Network.Wai.RequestSpec
Description : Top-level module for using RequestSpec
Copyright   : Allele Dev 2015
License     : BSD-3
Maintainer  : allele.dev@gmail.com
Stability   : experimental
Portability : POSIX
-}
module Network.Wai.RequestSpec (
  -- * Primitive parsing data types
  Result(..),
  P,

  -- * Primitive parsing functions
  parse,
  parseMaybe,
  parseEither,

  -- * Error generation and parser annotation
  (<?>),
  freeform,
  malformed,
  missing,

  -- * Error types
  Loc(..),
  Reason,
  Error(..),

  -- * Generating parsing environment
  toEnv,
  toEnvWithForm,
  toEnvRaw,

  -- * Parser driving type class
  Env,
  FromEnv(..),

  -- * Derived combinators, query parameters
  intQ,
  boolQ,
  floatQ,
  textQ,
  bytesQ,
  intQM,
  floatQM,
  textQM,
  bytesQM,

  -- * Derived combinators, form parameters
  intF,
  boolF,
  floatF,
  textF,
  bytesF,
  intFM,
  floatFM,
  textFM,
  bytesFM,

  -- * Derived combinators, headers
  intH,
  boolH,
  floatH,
  textH,
  bytesH,
  intHM,
  floatHM,
  textHM,
  bytesHM,

  -- * Derived combinators, utility
  choice
) where

import Network.Wai.RequestSpec.Class
import Network.Wai.RequestSpec.Error
import Network.Wai.RequestSpec.Combinators
import Network.Wai.RequestSpec.Parser
import Network.Wai.RequestSpec.Internal.Env
