{-|
Module      : Network.Wai.RequestSpec.Class
Description : FromEnv typeclass for parsing types from request
Copyright   : Allele Dev 2015
License     : BSD-3
Maintainer  : allele.dev@gmail.com
Stability   : experimental
Portability : POSIX

A typeclass for constructing data types from an environment. For more
details on environments, see "Network.Wai.RequestSpec.Internal.Env".
-}
module Network.Wai.RequestSpec.Class (
  FromEnv(..)
) where

import Network.Wai.RequestSpec.Internal.Env
import Network.Wai.RequestSpec.Internal.Parser

-- |
-- Allows for the parsing of a data type `a` from an Env
class FromEnv a where
  fromEnv :: Env -> P a
