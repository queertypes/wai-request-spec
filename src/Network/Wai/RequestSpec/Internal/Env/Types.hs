{-# LANGUAGE TypeFamilies, DataKinds #-}
module Network.Wai.RequestSpec.Internal.Env.Types (
  Env(..),
  EnvMap,
  Headers,
  ParamMap,
  QueryParams,
  FormParams,
  QParams,
  FParams,

  empty,
  lookup,
  fromList
) where

import Prelude hiding (lookup)
import Data.CaseInsensitive (CI)
import Data.Text (Text)
import qualified Data.Map as M

data QueryParams
data FormParams

type EnvMap k v  = M.Map k v
type Headers     = EnvMap (CI Text) Text
type ParamMap a  = EnvMap Text (Maybe Text)
type QParams     = ParamMap QueryParams
type FParams     = ParamMap FormParams

data Env = Env { headers :: Headers
               , qParams :: QParams
               , fParams :: FParams
               }

empty :: EnvMap k v
empty = M.empty

lookup :: (Ord k) => k -> EnvMap k v -> Maybe v
lookup = M.lookup

fromList :: (Ord k) => [(k,v)] -> EnvMap k v
fromList = M.fromList
