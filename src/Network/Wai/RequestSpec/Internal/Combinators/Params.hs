module Network.Wai.RequestSpec.Internal.Combinators.Params (
  qParam,
  qParamM,
  fParam,
  fParamM
) where

import Prelude hiding (lookup)
import Control.Applicative
import Data.Text (Text)

import Network.Wai.RequestSpec.Parser
import Network.Wai.RequestSpec.Error
import Network.Wai.RequestSpec.Internal.Env
import Network.Wai.RequestSpec.Internal.Env.Types

param :: (Env -> ParamMap a) -> (Text -> P a) -> Text -> Env -> P a
param params f s = maybe missingParam (there f) . lookup s . params
  where missingParam      = missing (Param s)
        there f' (Just t) = f' t
        there _ _         = missingParam

paramM :: (Env -> ParamMap a) -> (Text -> P a) -> Text -> Env -> P (Maybe a)
paramM params f s e = maybe (pure Nothing) (there f) $ lookup s (params e)
  where there f' (Just t) = f' t >>= (pure . Just)
        there _ _         = pure Nothing

qParam :: (Env -> ParamMap QueryParams) -> (Text -> P a) -> Text -> Env -> P a
qParam = param

qParamM :: (Env -> ParamMap QueryParams) -> (Text -> P a) -> Text -> Env -> P (Maybe a)
qParamM = paramM

fParam :: (Env -> ParamMap FormParams) -> (Text -> P a) -> Text -> Env -> P a
fParam = param

fParamM :: (Env -> ParamMap FormParams) -> (Text -> P a) -> Text -> Env -> P (Maybe a)
fParamM = paramM
