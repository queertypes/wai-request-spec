module Network.Wai.RequestSpec.Internal.Combinators.Params (
) where

import Control.Applicative
import Data.CaseInsensitive
import Data.Text (Text)
import Data.Text.Read
import qualified Data.Map as M

import Network.Wai.RequestSpec.Internal.Env
import Network.Wai.RequestSpec.Internal.Parser

param :: (Env -> ParamMap a) -> (Text -> P a) -> Text -> Env -> P a
param params f s = maybe missingParam (there f) . M.lookup s . params
  where missingParam      = missing (Param s)
        there f' (Just t) = f' t
        there _ _         = missingParam

paramM :: (Env -> ParamMap a) -> (Text -> P a) -> Text -> Env -> P (Maybe a)
paramM params f s e = maybe (pure Nothing) (there f) $ M.lookup s (params e)
  where there f' (Just t) = f' t >>= (pure . Just)
        there _ _         = pure Nothing

qParam_ :: (Env -> ParamMap QueryParams) -> (Text -> P a) -> Text -> Env -> P a
qParam_ = param

qParamM_ :: (Env -> ParamMap QueryParams) -> (Text -> P a) -> Text -> Env -> P (Maybe a)
qParamM_ = paramM

fParam_ :: (Env -> ParamMap FormParams) -> (Text -> P a) -> Text -> Env -> P a
fParam_ = param

fParamM_ :: (Env -> ParamMap FormParams) -> (Text -> P a) -> Text -> Env -> P (Maybe a)
fParamM_ = paramM
