{-# LANGUAGE OverloadedStrings  #-}
{-# LANGUAGE CPP  #-}
module Network.Wai.RequestSpec.Examples.Types where

#if __GLASGOW_HASKELL__ < 710
import Control.Applicative
#endif
import Prelude hiding (words)
import Data.ByteString hiding (filter)
import Data.Text hiding (filter)
import Data.Text.Encoding
import Network.Wai.RequestSpec
import qualified Data.ByteString.Base64 as B64
import qualified Data.Text as T

newtype Name = Name Text deriving (Show, Eq)
newtype Pass = Pass Text deriving (Show, Eq)
data Auth = Auth Name Pass deriving (Show, Eq)

instance FromEnv Auth where
  fromEnv e = do
      (name, pass) <- textH "Authorization" e >>= auth_
      return $ Auth name pass
    where b64decode = decodeUtf8 . B64.decodeLenient . encodeUtf8
          auth_ :: Text -> P (Name, Pass)
          auth_ a = case words a of
            ["Basic", rest] -> case (splitOn ":" . b64decode) rest of
              [u,p'] -> pure (Name u, Pass p')
              _     -> malformed "Could not parse Basic auth" rest
            _ -> malformed "Unknown Authorization format" a

newtype ClientId = ClientId Text deriving (Show, Eq)
newtype State = State ByteString deriving (Show, Eq)
newtype Scope = Scope [Text] deriving (Show, Eq)
newtype RedirectUri = RedirectUri Text deriving (Show, Eq)
data ResponseType
  = Code
  | Credentials
  | UnknownResponseType
  deriving (Show, Eq)

responseType :: Text -> P ResponseType
responseType "code" = pure Code
responseType "credentials" = pure Credentials
responseType t = malformed "Unknown response type" t

data OAuthorize =
  OAuthorize ClientId State Scope ResponseType (Maybe RedirectUri) Auth
  deriving (Show, Eq)

scope :: Text -> Scope
scope = Scope . filter (not . T.null) . T.splitOn ","

instance FromEnv OAuthorize where
  fromEnv e =
    OAuthorize <$> (ClientId <$> textQ "client_id" e)
               <*> (State <$> bytesQ enc "state" e)
               <*> (scope <$> textQ "scope" e)
               <*> (textQ "response_type" e >>= responseType)
               <*> (textQM "redirect_uri" e >>= (pure . maybe Nothing (Just . RedirectUri)))
               <*> fromEnv e
    where enc = encodeUtf8

-- used in Query data type
newtype Count = Count Int deriving Show
newtype Offset = Offset Int deriving Show
newtype UserName = UserName Text deriving Show
data Accepts = PlainText | JSON deriving Show
