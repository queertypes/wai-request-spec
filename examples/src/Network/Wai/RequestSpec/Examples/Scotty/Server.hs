{-# LANGUAGE OverloadedStrings #-}
module Main where

import Data.Text
import Data.Text.Lazy (fromStrict)
import Network.Wai.RequestSpec
import Web.Scotty

import Network.Wai.RequestSpec.Examples.Types

main :: IO ()
main = scotty 3000 $
  get "/auth" $ do
    req <- request
    let oauth = parse fromEnv (toEnv req) :: Result OAuthorize
    case oauth of
      Failure e -> text . fromStrict $ (pack . show $ e)
      Success v -> text (fromStrict . pack . show $ v)
