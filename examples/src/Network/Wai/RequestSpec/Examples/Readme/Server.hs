{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE CPP #-}
module Main (main) where

#if __GLASGOW_HASKELL__ < 710
import Control.Applicative
#endif

import Data.Text
import Data.Text.Lazy (fromStrict)
import Network.Wai.RequestSpec
import Network.HTTP.Types (badRequest400)
import Web.Scotty hiding (param, header)

import Network.Wai.RequestSpec.Examples.Types

------------------------------------------------------------
-- Your Data Model
------------------------------------------------------------
data Query =
  Query ClientId Count Offset UserName Accepts
  deriving Show

------------------------------------------------------------
-- Request Spec: The Part You Write
------------------------------------------------------------

-- if offset is given, return that value; else, offset is 0
offset :: Maybe Int -> Offset
offset (Just n) = Offset n
offset Nothing  = Offset 0

-- if Accept is given, parse it; otherwise, default to plain text
accept :: Maybe Text -> Accepts
accept (Just "text/plain")       = PlainText
accept (Just "application/json") = JSON
accept _                         = PlainText

instance FromEnv Query where
  fromEnv e =
    Query <$> (ClientId <$> textQ "client_id" e)
          <*> (Count <$> intQ "count" e)
          <*> (offset <$> intQM "offset" e)
          <*> (UserName <$> textH "X-User-Name" e)
          <*> (accept <$> textHM "Accept" e)

------------------------------------------------------------
-- Application: Making Use of Request Spec
------------------------------------------------------------

main :: IO ()
main = scotty 3000 $
  get "/query" $ do
    -- Request spec in action: parse what you want, and ...
    req <- request
    let query = parse fromEnv (toEnv req) :: Result Query

    -- figure out what to do next based on whether the request was valid!
    case query of
      -- show user what's missing
      Failure e -> status badRequest400 >> (text . fromStrict . pack . show $ e)

      -- show user what Query they made
      Success v -> text . fromStrict . pack . show $ v
