{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE CPP #-}
module Main where

import Data.Text
import Network.Wai.RequestSpec

#if __GLASGOW_HASKELL__ >= 800
import Web.Spock.Core
#else
import Web.Spock.Safe
#endif

import Network.Wai.RequestSpec.Examples.Types

main :: IO ()
main = runSpock 3000 $ spockT id $
  get "auth" $ do
    req <- request
    let oauth = parse fromEnv (toEnv req) :: Result OAuthorize
    case oauth of
      Failure e -> text (pack . show $ e)
      Success v -> text (pack . show $ v)
