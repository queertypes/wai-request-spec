{-# LANGUAGE OverloadedStrings #-}
module Main where

import Network.HTTP.Types
import Network.Wai
import Network.Wai.Handler.Warp
import Network.Wai.RequestSpec
import qualified Data.ByteString.Lazy as LB
import qualified Data.ByteString.Lazy.Char8 as LBC

import Network.Wai.RequestSpec.Examples.Types

textContentType :: ResponseHeaders
textContentType = [(hContentType, "text/plain")]

badReq :: (Response -> t) -> LB.ByteString -> t
badReq f msg = f $ responseLBS badRequest400 textContentType msg

goodReq :: (Response -> t) -> LB.ByteString -> t
goodReq f msg = f $ responseLBS status200 textContentType msg

authorize :: Application
authorize req f = do
  let oauth = parse fromEnv (toEnv req) :: Result OAuthorize
  case oauth of
   Failure e -> badReq f (LBC.pack . show $ e)
   Success v -> goodReq f (LBC.pack . show $ v)

app :: Application
app req f =
  case (requestMethod req, pathInfo req) of
   ("GET", ["auth"]) -> authorize req f
   ("GET", _) -> f $ responseLBS notFound404 textContentType ""
   (_, ["auth"]) -> f $ responseLBS methodNotAllowed405 textContentType ""
   (_,_) -> badReq f ""

main :: IO ()
main = run 3000 app
